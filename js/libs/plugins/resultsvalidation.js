

define(['jquery', 'jsvalidate'], function($, JSValidate){
  $(document).ready(function() {
    $("#results-form").validate({
      rules: {
        resultCount:{
          required: true,
          min: 100,
          max: 10000
        },
        field: {
          required: true,
        },
        group: {
          required: true,
        }

      },
      messages: {
        resultCount: {
          required: "Please enter your password",
          max: "Your password must be at least 5 characters long"
        },
        field: {
          required: "Please select one item from the group"
        },
        group: {
          required: "Please select one item from the group"
        },

      }
    });

  });
});